import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:google_generative_ai/google_generative_ai.dart';
import 'package:image_picker/image_picker.dart';
import 'package:learn_hydrated_bloc/constant/constant.dart';

class GeminiPromptPage extends StatefulWidget {
  const GeminiPromptPage({super.key});

  @override
  State<GeminiPromptPage> createState() => _GeminiPromptPageState();
}

class _GeminiPromptPageState extends State<GeminiPromptPage> {
  GenerativeModel model = GenerativeModel(
    model: 'gemini-1.5-flash-latest',
    apiKey: geminiKey,
  );

  TextEditingController controller = TextEditingController();
  bool isLoading = false;
  List<String> results = [];
  XFile? image;

  void onSubmitPrompt(String prompt) {
    setState(() {
      isLoading = true;
      controller.clear();
    });
    model.generateContent([
      // Content.text(prompt),
      Content.multi([
        TextPart(controller.text),
        if (image != null)
          DataPart(
            'image/jpeg',
            File(image!.path).readAsBytesSync(),
          )
      ])
    ]).then((value) {
      setState(() {
        results.add(value.text.toString());
        isLoading = false;
      });
    });
  }

  void onResetPrompt() {
    setState(() {
      image = null;
      results.clear();
      controller.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('AI Prompt'),
        actions: [
          IconButton(
            onPressed: () {
              onResetPrompt();
            },
            icon: const Icon(Icons.restart_alt),
          )
        ],
      ),
      bottomNavigationBar: SafeArea(
        child: Row(
          children: [
            IconButton(
              onPressed: () async {
                ImagePicker()
                    .pickImage(source: ImageSource.gallery)
                    .then((value) {
                  setState(() {
                    image = value;
                  });
                });
              },
              icon: const Icon(Icons.add),
            ),
            Flexible(
              child: TextField(
                controller: controller,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your prompt request...',
                ),
                onSubmitted: (value) {
                  onSubmitPrompt(controller.text);
                },
              ),
            ),
            IconButton(
              onPressed: () {
                onSubmitPrompt(controller.text);
              },
              icon: const Icon(Icons.send),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            if (image != null)
              Image.asset(
                image!.path,
                width: MediaQuery.sizeOf(context).width,
                height: 300,
                fit: BoxFit.cover,
              ),
            isLoading
                ? const Center(
                    child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: CircularProgressIndicator(),
                  ))
                : ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: results.length,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24.0,
                      vertical: 12,
                    ),
                    itemBuilder: (context, index) {
                      if ((index + 1 == results.length) && isLoading) {
                        return const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        );
                      } else {
                        return MarkdownBody(data: results[index]);
                      }
                    },
                  ),
          ],
        ),
      ),
    );
  }
}
