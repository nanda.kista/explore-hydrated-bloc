import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learn_hydrated_bloc/pages/user_detail/user_detail_page.dart';
import 'package:learn_hydrated_bloc/pages/user_list/bloc/user_list_bloc.dart';

class UserListPage<T extends UserListBloc> extends StatelessWidget {
  const UserListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<T, UserListState>(
      builder: (BuildContext context, state) {
        return switch (state) {
          UserListLoading() => const Center(
            child: CircularProgressIndicator(),
          ),
          UserListError() => Center(
            child: Text('Error ${state.message}'),
          ),
          UserListLoaded() => ListView.builder(
            itemCount: state.data.length,
            itemBuilder: (context, index) {
              final user = state.data[index];
              return ListTile(
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(user.avatar),
                  radius: 24,
                ),
                title: Text('${user.firstName} ${user.lastName}'),
                subtitle: Text(user.email),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => UserDetailPage(
                        id: user.id,
                        name: user.lastName,
                      ),
                    ),
                  );
                },
              );
            },
          ),
          _ => const SizedBox.shrink(),
        };
      },
    );
  }
}
