part of 'user_list_bloc.dart';

sealed class UserListEvent extends Equatable {
  const UserListEvent();

  @override
  List<Object?> get props => [];
}

class LoadUsersList extends UserListEvent {
  final int page;

  const LoadUsersList(this.page);

  @override
  List<Object?> get props => [page];
}
