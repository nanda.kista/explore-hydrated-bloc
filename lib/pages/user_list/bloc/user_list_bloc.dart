import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:learn_hydrated_bloc/models/user.dart';

part 'user_list_state.dart';

part 'user_list_event.dart';

class UserListBloc extends HydratedBloc<UserListEvent, UserListState> {
  UserListBloc() : super(UserListInitial()) {
    on<LoadUsersList>(_onLoadData);

    // add(LoadUsersList());
  }

  @override
  fromJson(Map<String, dynamic> json) {
    return UserListLoaded(
      (json['cache'] as List? ?? []).map((e) => User.fromJson(e)).toList(),
    );
  }

  @override
  Map<String, dynamic>? toJson(state) {
    return (state is UserListLoaded)
        ? {'cache': List.from(state.data.map((e) => e.toJson()))}
        : null;
  }

  void _onLoadData(LoadUsersList event, Emitter<UserListState> emit) async {
    try {
      if (state is UserListInitial) emit(UserListLoading());
      final response = await Dio().get(
        'https://reqres.in/api/users',
        queryParameters: {
          'page': event.page,
        },
      );

      List<User> data = List<User>.from((response.data['data'] as List).map(
        (e) => User.fromJson(e),
      ));
      emit(UserListLoaded(data));
    } catch (e, stackTrace) {
      log('Error $e, $stackTrace');
      emit(UserListError(e.toString()));
    }
  }
}
