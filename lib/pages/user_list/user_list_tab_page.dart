import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:learn_hydrated_bloc/pages/prompt/gemini_prompt_page.dart';
import 'package:learn_hydrated_bloc/pages/user_list/bloc/user_list_bloc.dart';
import 'package:learn_hydrated_bloc/pages/user_list/bloc/user_list_tab_bloc.dart';
import 'package:learn_hydrated_bloc/pages/user_list/user_list_page.dart';

class UserListTabPage extends StatelessWidget {
  const UserListTabPage({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => UserListOneBloc()..add(const LoadUsersList(1)),
        ),
        BlocProvider(
          create: (_) => UserListTwoBloc()..add(const LoadUsersList(2)),
        ),
      ],
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Github Users'),
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const GeminiPromptPage()),
                  );
                },
                icon: const Icon(Icons.message_outlined),
              ),
            ],
            bottom: const TabBar(
              tabs: [
                Tab(text: 'Nanda'),
                Tab(text: 'Permana'),
              ],
            ),
          ),
          body: const TabBarView(
            children: [
              UserListPage<UserListOneBloc>(),
              UserListPage<UserListTwoBloc>(),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              await HydratedBloc.storage.clear();
            },
            child: const Icon(Icons.delete),
          ),
        ),
      ),
    );
  }
}
