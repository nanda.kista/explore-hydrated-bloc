import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:learn_hydrated_bloc/models/user.dart';

part 'user_detail_state.dart';

class UserDetailCubit extends HydratedCubit<UserDetailState> {
  final int userId;

  UserDetailCubit(this.userId) : super(UserDetailInitial()) {
    loadUser();
  }

  @override
  String get id => userId.toString();

  @override
  UserDetailState? fromJson(Map<String, dynamic> json) {
    return UserDetailLoaded(User.fromJson(json['cache']));
  }

  @override
  Map<String, dynamic>? toJson(UserDetailState state) {
    return (state is UserDetailLoaded) ? {'cache': state.data.toJson()} : null;
  }

  void loadUser() async {
    try {
      if (state is UserDetailInitial) emit(UserDetailLoading());
      final response = await Dio().get('https://reqres.in/api/users/$userId');
      User data = User.fromJson(response.data['data']);
      emit(UserDetailLoaded(data));
    } catch (e, stackTrace) {
      log('Error $e, $stackTrace');
      emit(UserDetailError(e.toString()));
    }
  }
}
