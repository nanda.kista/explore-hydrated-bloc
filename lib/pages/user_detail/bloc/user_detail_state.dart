part of 'user_detail_cubit.dart';

@immutable
sealed class UserDetailState extends Equatable {
  @override
  List<Object?> get props => [];
}

class UserDetailInitial extends UserDetailState {}

class UserDetailLoading extends UserDetailState {}

class UserDetailLoaded extends UserDetailState {
  final User data;

  UserDetailLoaded(this.data);

  @override
  List<Object?> get props => [data];
}

class UserDetailError extends UserDetailState {
  final String message;

  UserDetailError(this.message);

  @override
  List<Object?> get props => [message];
}
