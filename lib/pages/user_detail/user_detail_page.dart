import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/user_detail_cubit.dart';

class UserDetailPage extends StatelessWidget {
  const UserDetailPage({super.key, required this.id, required this.name});

  final int id;
  final String name;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserDetailCubit(id),
      child: Scaffold(
        appBar: AppBar(
          title: Text(name),
        ),
        body: BlocBuilder<UserDetailCubit, UserDetailState>(
            builder: (context, state) {
          return switch (state) {
            UserDetailLoading() =>
              const Center(child: CircularProgressIndicator()),
            UserDetailError() => Center(
                child: Text(state.message),
              ),
            UserDetailLoaded() => Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      backgroundImage: NetworkImage(state.data.avatar),
                      radius: 120,
                    ),
                    const SizedBox(height: 24),
                    Text(
                      '${state.data.firstName} ${state.data.lastName}',
                      style: Theme.of(context).textTheme.headlineSmall,
                    ),
                    const SizedBox(height: 12),
                    Text(
                      state.data.email,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
              ),
            _ => const SizedBox.shrink()
          };
        }),
      ),
    );
  }
}
